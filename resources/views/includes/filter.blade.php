						<div class="item-filter">
							@php
							$locationActive = session('location');
							@endphp
							<ul class="filter-list">
								<li class="item-short-area">
									<p>{{$langg->lang64}} :</p>
									<select id="sortby" name="sort" class="short-item">


										{!! !empty($locationActive) ? "<option value='nearest'>$langg->lang808</option>" : null !!}
										<option value="date_desc">{{$langg->lang65}}</option>
										<option value="date_asc">{{$langg->lang66}}</option>
										<option value="price_asc">{{$langg->lang67}}</option>
										<option value="price_desc">{{$langg->lang68}}</option>
										{!! !empty($locationActive) ? "<option value='farthest'>$langg->lang809</option>" : null !!}
									</select>
								</li>
							</ul>
						</div>