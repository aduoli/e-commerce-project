@foreach($prods as $prod)
	<div class="docname">
		<a href="{{ route('front.product', $prod->slug) }}" class="d-flex align-items-center">
			<img src="{{ Storage::disk('dos')->url('assets/images/thumbnails/'.$prod->thumbnail) }}" alt="">
			<div class="search-content">
				<div class="d-flex">
					<div style="margin-right: auto;">
						<p>{!! mb_strlen($prod->name,'utf-8') > 66 ? str_replace($slug,'<b>'.$slug.'</b>',mb_substr($prod->name,0,66,'utf-8')).'...' : str_replace($slug,'<b>'.$slug.'</b>',$prod->name)  !!} </p>
						<span style="font-size: 14px; font-weight:600; display:block;">{{ $prod->showPrice() }}</span>
					</div>
					<p class="text-black-50 d-flex justify-content-center align-items-center font-italic"><small>{{ isset($prod->distance) ? round($prod->distance, 1) . 'km' : '&#9867;&#9900;&#9867;' }} </small></p>
				</div>
			</div>
		</a>
	</div> 
@endforeach