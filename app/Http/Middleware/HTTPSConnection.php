<?php

namespace App\Http\Middleware;
use Closure;
use App\Models\Generalsetting;
use Illuminate\Support\Facades\App;

class HTTPSConnection {
    public function handle($request, Closure $next)

    {
        $gs = Generalsetting::find(1);
        
            if($gs->is_secure == 1) {
                $environment = App::environment();                
                if (!$request->secure() && $environment == 'production') {

                    return redirect()->secure($request->getRequestUri());
                }
            }


            return $next($request);

    }

}



?>